package com.equeue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EQueueTelegramBotApplication {
	public static void main(String[] args) {
		SpringApplication.run(EQueueTelegramBotApplication.class, args);
	}
	// testing project build by Jenkins, attempt 14
	// testing project build by Jenkins after merge request, attempt 1
	// testing deploy, attempt 9
}
